﻿using UnityEngine;

public class ObjectsDragger : MonoBehaviour
{
    private IDraggable draggableObject;
    private bool isActive = true;

    public Transform DraggableObject
    {
        get
        {
            if (draggableObject == null) return null;

            return draggableObject.DraggableObject;
        }
    }

    public bool IsActive => isActive;

    private void Update()
    {
        if (draggableObject == null) return;

        if(draggableObject.IsActive == false)
        {
            draggableObject = null;
        }
    }

    public bool TryTakeShell(RaycastHit hit)
    {
        draggableObject = hit.transform.GetComponent<IDraggable>();
        return DraggableObject != null;
    }

    public void ResetDraggingObject() => draggableObject = null;

    public void Activate() => isActive = true;
    public void Deactivate() => isActive = false;
}