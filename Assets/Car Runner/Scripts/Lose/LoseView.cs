﻿using UnityEngine;
using UnityEngine.UI;

public class LoseView : MonoBehaviour
{
    [SerializeField] private Lose lose;
    [SerializeField] private GameObject loseWindow;
    [SerializeField] private Button restartButton;

    private void Awake()
    {
        lose.OnLose += ShowWindow;
        restartButton.onClick.AddListener(lose.Restart);
    }

    private void ShowWindow()
    {
        loseWindow.SetActive(true);
    }

    private void OnDestroy()
    {
        if (lose == null) return;

        lose.OnLose -= ShowWindow;
    }
}