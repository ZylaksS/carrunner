﻿using UnityEngine;

public class LoseMediator : MonoBehaviour
{
    [SerializeField] private Lose lose;
    [SerializeField] private CarMoveView carMoveView;

    private void Awake()
    {
        carMoveView.OnObstacleTriggered += lose.HandleLose;
    }

    private void OnDestroy()
    {
        if (carMoveView == null) return;

        carMoveView.OnObstacleTriggered -= lose.HandleLose;
    }
}