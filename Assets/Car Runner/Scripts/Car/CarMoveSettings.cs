﻿using UnityEngine;

[CreateAssetMenu(fileName = "Car Move Settings", menuName = "Car Runner/Settings/Car Move")]
public class CarMoveSettings : ScriptableObject
{
    [SerializeField] private float moveSpeed;

    public float MoveSpeed => moveSpeed;
}