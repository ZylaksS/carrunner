﻿using UnityEngine;

public class CarMoveData : MonoBehaviour
{
    [SerializeField] private CarMoveSettings carMoveSettings;

    public CarMoveType CarMoveType { get; private set; } = CarMoveType.Move;
    public float CurrentSpeed { get; private set; }

    public void Stop() => CarMoveType = CarMoveType.Stop;

    private void Awake()
    {
        CurrentSpeed = carMoveSettings.MoveSpeed;
    }
}