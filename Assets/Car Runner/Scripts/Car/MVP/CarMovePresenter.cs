﻿using UnityEngine;

public class CarMovePresenter : MonoBehaviour
{
    [SerializeField] private CarMoveData carMoveData;
    [SerializeField] private CarMoveView carMoveView;

    private void Awake()
    {
        carMoveView.OnObstacleTriggered += Stop;
    }

    private void Update()
    {
        if (carMoveData.CarMoveType == CarMoveType.Stop) return;

        carMoveView.Move(carMoveData.CurrentSpeed);
    }

    public void Stop()
    {
        UnsubscribeFromView();
        carMoveData.Stop();
    }

    private void UnsubscribeFromView()
    {
        if (carMoveView == null) return;

        carMoveView.OnObstacleTriggered -= Stop;
    }

    private void OnDestroy()
    {
        UnsubscribeFromView();
    }
}