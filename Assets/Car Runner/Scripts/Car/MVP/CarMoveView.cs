﻿using System;
using UnityEngine;

public class CarMoveView : MonoBehaviour, IPlayer
{
    public event Action OnObstacleTriggered;

    public void Move(float speed)
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        var obstacle = other.GetComponent<IObstacle>();

        if (obstacle == null) return;

        OnObstacleTriggered?.Invoke();
    }
}