﻿using UnityEngine;

public class CarMoveMediator : MonoBehaviour
{
    [SerializeField] private CarMovePresenter carMove;
    [SerializeField] private Finish finish;

    private void Awake()
    {
        finish.OnFinished += carMove.Stop;
    }

    private void OnDestroy()
    {
        finish.OnFinished -= carMove.Stop;
    }
}