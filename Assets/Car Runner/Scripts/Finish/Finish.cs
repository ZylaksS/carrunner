﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour
{
    public event Action OnFinished;

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<IPlayer>();

        if (player == null) return;

        OnFinished?.Invoke();
    }
}