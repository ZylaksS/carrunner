﻿using UnityEngine;
using UnityEngine.UI;

public class FinishView : MonoBehaviour
{
    [SerializeField] private Finish finish;
    [SerializeField] private GameObject finishWindow;
    [SerializeField] private Button restartButton;

    private void Awake()
    {
        finish.OnFinished += ShowWindow;
        restartButton.onClick.AddListener(finish.Restart);
    }

    private void ShowWindow()
    {
        finishWindow.SetActive(true);
    }

    private void OnDestroy()
    {
        if (finish == null) return;

        finish.OnFinished -= ShowWindow;
    }
}