﻿using UnityEngine;

public class Obstacle : MonoBehaviour, IObstacle, IDraggable
{
    [SerializeField] private Transform obstacle;

    public Transform DraggableObject => obstacle;

    public bool IsActive { get; private set; } = true;

    public void Deactivate()
    {
        IsActive = false;
    }
}