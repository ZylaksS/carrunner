﻿using UnityEngine;
using UnityEngine.UI;

public class RoadView : MonoBehaviour
{
    [SerializeField] private Road road;
    [SerializeField] private Text obstaclesOnRoadCount;
    [SerializeField] private string prefix;

    private void Awake()
    {
        road.OnObstaclesCountChanged += DisplayObstaclesCount;
    }

    private void Start()
    {
        DisplayObstaclesCount(road.DeletedObstaclesCount);
    }

    private void DisplayObstaclesCount(int value)
    {
        obstaclesOnRoadCount.text = $"{prefix} {value}";
    }

    private void OnDestroy()
    {
        road.OnObstaclesCountChanged -= DisplayObstaclesCount;
    }
}