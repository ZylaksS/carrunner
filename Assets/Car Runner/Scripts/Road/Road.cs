﻿using System;
using UnityEngine;

public class Road : MonoBehaviour
{
    public int DeletedObstaclesCount { get; private set; }

    public event Action<int> OnObstaclesCountChanged;

    private void OnTriggerExit(Collider other)
    {
        var obstacle = other.GetComponent<IObstacle>();

        if (obstacle == null) return;

        obstacle.Deactivate();

        DeletedObstaclesCount++;
        OnObstaclesCountChanged?.Invoke(DeletedObstaclesCount);
    }
}